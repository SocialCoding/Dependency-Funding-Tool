def crawl_urls(locations, fn):
    def crawler(unchecked, result):
        unchecked_, result_

    with multiprocessing.Manager() as manager:
        unchecked = manager.dict({ location: None for location in locations })

        result = manager.dict()

        with multiprocessing.Pool(processes) as pool:
            pool.map(crawler, [(unchecked, result) for _ in range(processes)])

        return result
