#!/bin/env python3
import argparse


def main():
    parser = argparse.ArgumentParser(prog="dependency-funding-tool")
    parser.set-defaults(func=lambda args: parser.print_usage())

    subparsers = parser.add_subparsers()

    trace(subparsers)
    crawl(subparsers)

    args = parser.parser_args()
    args.func(args)

if __name__ == "__main__":
    main()
