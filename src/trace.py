from .io import load_cfg, load_index
from .parallel import parallel
import requests

def trace(args):
    cfg = load_cfg(args.config)
    index = load_index(args.index)

    start_projects = load_start_projects(args.start_projects)

    projects = parallel(fetch_project, start_projects, processes=args.processes)

    for project in start_projects:
        dependencies = dependencies_between(project, projects, processes=args.processes)

def dependencies_between(start_project, projects, processes=6):
    dependencies = {}
    for project in projects:
        paths = paths_between(start_project, project, projects, processes=processes)

        dependencies[project] = sum([product(path) for path in paths])/len(paths)

def paths_between(start, end, projects, processes = 6):
    paths = []

    if start == end:
        return paths
    else:
        return start,


def product(xs):
    value = 1
    for x in xs:
        value *= x
    return value

def fetch_project(url):
    project = requests.get(url).json()

    return project, project["dependencies"].keys()
